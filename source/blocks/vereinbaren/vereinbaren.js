$(document).ready(function () {
  /* ============================================================================
    * popup
    * ============================================================================ */
  $('.main-page-vereinbaren__calendar-infoform-wrapper button').on('click', function (event) {
    event.preventDefault();
    // $('body').css('overflow', 'hidden');
    popup1.removeClass('hidden');
    setTimeout(function () {
      popup1.removeClass('visuallyhidden');
    }, 20);
  });
  var popup1 = $('#formSendPopup');
  $('#formSendPopup .btn, img').on('click', function () {
    popup1.addClass('visuallyhidden');
    popup1.one('transitionend', function (e) {
      popup1.addClass('hidden');
    });
  });
});