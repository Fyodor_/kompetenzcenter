
  var map;
  var marker;
  var infowindow;

  function initMap() {
    if ($("#map").length > 0){
    map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: 50.912362,
        lng: 6.967732
      },
      zoom: 13
    });
  }
    marker = new google.maps.Marker({
      position: {
        lat: 50.912362,
        lng: 6.967732
      },
      map: map,
      animation: google.maps.Animation.DROP,
    });
    marker.addListener('click', toggleBounce);
    infowindow = new google.maps.InfoWindow({
      content: 'Koblenzer Straße 88, 50968 Köln'
    });

  }

  function toggleBounce() {
    infowindow.open(map, marker);

    if (marker.getAnimation() !== null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
  }
