$(document).ready(function () {
  if ($('section').hasClass("product-page")) {
    changeSearchCityPosition();
    $(window).resize(
      function () {
        changeSearchCityPosition();
      }
    )
  }
});
function changeSearchCityPosition() {
  if ($(window).width() < 767) {
    $('h1').insertAfter($('.product-page__product-mainpart-img'));
  } else {
    $('h1').prependTo($('.product-page'));
  }
}