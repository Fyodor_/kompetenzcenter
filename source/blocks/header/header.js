$(document).ready(function () {
  /* ============================================================================
   * прокрутка до id
   * ============================================================================ */
  $('a[href^="#"], *[data-href^="#"]').on('click', function (e) {
    e.preventDefault();
    var t = 1000;
    var d = $(this).attr('data-href') ? $(this).attr('data-href') : $(this).attr('href');
    //window.location.pathname=='/'
    if ($('section').hasClass('main-page-slider')) {
      $('html,body').stop().animate({
        scrollTop: $(d).offset().top
      }, t);
    } else {
      window.location.href = '/' + d;
    }
  });

  /* ============================================================================
   * burger
   * ============================================================================ */
  $('button.burger').on('click', function () {
    if ($('.burger').hasClass('burger--close')) {
      $('body').css('overflow', 'hidden');
    } else {
      $('body').css('overflow', 'auto');
    }
  });
  var box = $('#header-burger');
  $('button.burger').on('click', function () {
    if (box.hasClass('hidden')) {
      box.removeClass('hidden');
      setTimeout(function () {
        box.removeClass('visuallyhidden');
      }, 400);
    } else {
      box.addClass('visuallyhidden');
      box.one('transitionend', function (e) {
        box.addClass('hidden');
      });
    }
  });
  $('#header-burger a').on('click', function () {
    $('body').css('overflow', 'auto');
    $('.burger').removeClass('burger--close');
    box.addClass('visuallyhidden');
    box.one('transitionend', function (e) {
      box.addClass('hidden');
    });
  });

});